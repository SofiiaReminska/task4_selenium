package com.epam.lab.bo;

import com.epam.lab.po.LoginPage;

import static com.epam.lab.utils.DriverManager.getDriver;

public class LoginBO {
    private LoginPage loginPage;

    public LoginBO() {
        loginPage = new LoginPage(getDriver());
    }

    public void login(String email, String password) {
        loginPage.enterEmail(email);
        loginPage.clickIdentifierNextButton();
        loginPage.waitForEnterPassword();
        loginPage.enterPassword(password);
        loginPage.clickPasswordNextButton();
        loginPage.waitForNextButtonToBeInvisible();
    }
}
